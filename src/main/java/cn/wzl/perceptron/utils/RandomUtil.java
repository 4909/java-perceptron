package cn.wzl.perceptron.utils;

import java.util.Random;

public class RandomUtil {

    private static final Random random = new Random();

    public static double randomDouble() {
        return random.nextDouble();
    }
}
