package cn.wzl.perceptron.utils;

public class ClassTransferUtil {

    public static double[] transferBooleansToDoubles(boolean[] booleans){
        double[] doubles = new double[booleans.length];
        for(int i = 0;i < booleans.length; i ++) {
            doubles[i] = transferBooleanToDouble(booleans[i]);
        }
        return doubles;
    }

    public static double transferBooleanToDouble(Boolean aBoolean){
        return aBoolean ? 1.0 : 0.0;
    }
}
