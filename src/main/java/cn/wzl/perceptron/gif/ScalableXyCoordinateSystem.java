package cn.wzl.perceptron.gif;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ScalableXyCoordinateSystem {

    private final XyCoordinateSystem xyCoordinateSystem;
    private final double scalableRate;


    public ScalableXyCoordinateSystem (int maxX, int maxY, double scalableRate) {
        this.scalableRate = scalableRate;
        xyCoordinateSystem = new XyCoordinateSystem((int)(maxX*scalableRate), (int)(maxY*scalableRate));
    }

    public void drawLine(double w1, double w2, double b, Color color) {
        Graphics2D pen = xyCoordinateSystem.retrievePen(color);
        xyCoordinateSystem.drawLineWithWAndB(pen, w1, w2, b*scalableRate);
    }

    public void drawPoint(double x, double y, Color color) {
        Graphics2D pen = xyCoordinateSystem.retrievePen(color);
        xyCoordinateSystem.drawPoint(pen, x*scalableRate, y*scalableRate, 0.2 * scalableRate);
    }

    public void writeToFile(String fileName) throws IOException {
        xyCoordinateSystem.writeToFile(fileName);
    }

    public BufferedImage retrieveBufferedImage() {
        return xyCoordinateSystem.bi;
    }

    public void drawTitle(double x, double y, String title) {
        Graphics2D pen = xyCoordinateSystem.retrievePen(Color.BLACK);
        xyCoordinateSystem.drawString(pen, title, x*scalableRate, y*scalableRate);
    }
}
