package cn.wzl.perceptron;

import cn.wzl.perceptron.gif.ScalableXyCoordinateSystem;
import cn.wzl.perceptron.perceptron.AndPerceptron;

import java.awt.*;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        trainAndPerceptron();
    }

    public static void trainAndPerceptron() {
        AndPerceptron andPerceptron = new AndPerceptron(false);

        for(int i = 0; i < 8; i ++) {
            andPerceptron.training(new boolean[]{true, true}, true);
            andPerceptron.training(new boolean[]{true, false}, false);
            andPerceptron.training(new boolean[]{false, true}, false);
            andPerceptron.training(new boolean[]{false, false}, false);
        }

        andPerceptron.generateDebugGif("output/gif/1.gif");

        System.out.println(andPerceptron.run(new boolean[]{true, true}));
        System.out.println(andPerceptron.run(new boolean[]{true, false}));
        System.out.println(andPerceptron.run(new boolean[]{false, true}));
        System.out.println(andPerceptron.run(new boolean[]{false, false}));
    }

    public static void drawCoordinateSystem() throws IOException {
        ScalableXyCoordinateSystem scalableXyCoordinateSystem = new ScalableXyCoordinateSystem(5, 5, 50);
        scalableXyCoordinateSystem.drawPoint(1, 1, Color.BLUE);
        scalableXyCoordinateSystem.drawPoint(1, 0, Color.BLACK);
        scalableXyCoordinateSystem.drawPoint(0, 1, Color.BLACK);
        scalableXyCoordinateSystem.drawPoint(0, 0, Color.BLACK);

        scalableXyCoordinateSystem.drawLine(1, 1, 0, Color.RED);

        scalableXyCoordinateSystem.writeToFile("output/image/1.jpg");
    }
}
